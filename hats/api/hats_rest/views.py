from django.shortcuts import render
from common.json import ModelEncoder
from hats_rest.models import LocationVO
from hats_rest.models import Hat
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

class LocationVOencoder(ModelEncoder):
    model = LocationVO
    properties = ["closet_name", "import_href", "section_number","shelf_number"]



class HatEncoder(ModelEncoder):
    model = Hat
    properties = [
        "id",
        "fabric",
        "style_name",
        "color",
        "picture_url",
        "location",
    ]
    encoders = {"location": LocationVOencoder(),}
    



#Hats List View, GET list and Add to list
@require_http_methods(["GET", "POST"])
def api_list_hats(request):
    if request.method == "GET":
        hats = Hat.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatEncoder,
        )
    else: #POST
        content = json.loads(request.body)
        href = content["location"]
        location = LocationVO.objects.get(import_href=href)
        content["location"] = location
        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatEncoder,
            safe=False,
        )

@require_http_methods(["DELETE"])
def delete_hat(request, pk):
    if request.method == "DELETE":
        try:
            hat = Hat.objects.get(id=pk)
            hat.delete()
            return JsonResponse(
                hat,
                encoder=HatEncoder,
                safe=False,
            )
        except Hat.DoesNotExist:
            return JsonResponse({"message": "This Hat Does not exist, failed to delete"})
    