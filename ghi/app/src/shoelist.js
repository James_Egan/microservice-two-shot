async function deleteShoe(shoe) {
    const deleteUrl = `http://localhost:8080/api/shoes/${shoe.id}`
    const fetchConfig = {
      method: "DELETE"
    }
    const response = await fetch(deleteUrl, fetchConfig)
    
      window.location.reload()
      
    
}
  





function ShoeList(props) {
    return (
      <>
      <h1>Welcome to your list of Shoes</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Manufacturer</th>
            <th>Model Name</th>
            <th>Color</th>
            <th>Closet Name</th>
            <th>Delete?</th>
          </tr>
        </thead>
        <tbody>
          {props.shoes.map(shoe => {
            return (
              <tr key={ shoe.id }>
                <td>{ shoe.manufacturer }</td>
                <td>{ shoe.model_name }</td>
                <td>{ shoe.color }</td>
                <td>{ shoe.bin.closet_name }</td>
                <td><button className="btn btn-danger" onClick={ () => deleteShoe(shoe)}>Delete</button></td>
              </tr>
            );
          })}
        </tbody>
      </table>
      </>
    );
  }
  
  
  export default ShoeList;
  