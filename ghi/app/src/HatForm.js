import React from 'react';

class HatForm extends React.Component {
    constructor(props) {
      super(props)
      this.state = {
        style_name: '',
        fabric: '',
        color: '',
        location: '',
        locations: [],
       
      };
      this.handleStyleNameChange = this.handleStyleNameChange.bind(this);
      this.handleFabricChange = this.handleFabricChange.bind(this);
      this.handleColorNameChange = this.handleColorNameChange.bind(this);
      this.handleLocationChange = this.handleLocationChange.bind(this);
      this.handleSubmit = this.handleSubmit.bind(this);
    }
    handleStyleNameChange(event) {
        const value = event.target.value;
        this.setState({style_name: value})
      }
      handleFabricChange(event) {
    const value = event.target.value;
    this.setState({fabric: value})
    }
    handleColorNameChange(event){
        const value = event.target.value;
        this.setState({color: value})
    }
    handleLocationChange(event){
        const value = event.target.value;
        this.setState({location: value})
    }
    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        delete data.locations;
        console.log(data);
      
        const hatUrl = 'http://localhost:8090/api/hats/';
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };
        const response = await fetch(hatUrl, fetchConfig);

        //If the response is ok and Submit goes through, clear the form using this code.
        // In learn Submitting a form day 1 week 10
        if (response.ok) {
            const newLocation = await response.json();
            console.log(newLocation);
        
            const cleared = {
                style_name: '',
                fabric: '',
                color: '',
                location: '',
                locations: [],
            };
            this.setState(cleared);
            window.location.reload()

          }
      }
    


   async componentDidMount() {
      const url = 'http://localhost:8100/api/locations/';
  
      const response = await fetch(url);
  
      if (response.ok) {
        const data = await response.json();
        
        this.setState({locations: data.locations});
        console.log(data.locations)
        
      }
    }
  
  render() {
    return (
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Create New Hat!</h1>
              <form onSubmit={this.handleSubmit} id="create-hat-form">
                <div className="form-floating mb-3">
                <input value={this.state.style_name} onChange={this.handleStyleNameChange} placeholder="Style Name" required
                    type="text" name="style_name" id="style_name"
                    className="form-control" />
                  <label htmlFor="name">Style Name</label>
                </div>
                <div className="form-floating mb-3">
                <input value={this.state.fabric} onChange={this.handleFabricChange} placeholder="Fabric Type"
                required type="text" name="fabric" id="fabric"
                className="form-control" />
                  <label htmlFor="fabric">Fabric Type</label>
                </div>
                <div className="form-floating mb-3">
                <input value={this.state.color} onChange={this.handleColorNameChange} placeholder="color" required
                    type="text" name="color" id="color"
                    className="form-control" />
                  <label htmlFor="city">Color</label>
                </div>

                <div className="mb-3">
                <select onChange={this.handleLocationChange} required name="location" id="location" className="form-select">
                <option value={this.state.locations}>Choose a location</option>
                    {this.state.locations.map(location => {

                        return (
                            <option key={location.href} value={location.href}>
                                
                                {location.closet_name} 
                                
                            </option>
                        );
                    })}
                </select>
                </div>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      );
    }
  }

export default HatForm;