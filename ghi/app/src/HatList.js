  async function deleteHat(hat) {
    const deleteUrl = `http://localhost:8090/api/hats/${hat.id}`
    const fetchConfig = {
      method: "DELETE"
    }
    const response = await fetch(deleteUrl, fetchConfig)
    window.location.reload()
      
      
    
}
  
function HatList(props) {
    return (
        <>
        <h1>Welcome to your list of Hats</h1>
      <table className="table table-striped">
        <thead>
        <tr>
            <th>Style Name</th>
            <th>Fabric</th>
            <th>Color</th>
            <th>Location in Wardrobe</th>
            <th>Delete?</th>
          </tr>
        </thead>
        <tbody>
          {props.hats.map(hat => {
            return (
            <tr key={hat.id}>
                <td>{ hat.style_name }</td>
                <td>{ hat.fabric }</td>
                <td>{ hat.color }</td>
                <td>{ hat.location.closet_name }</td>
                <td><button className="btn btn-danger" onClick={ () => deleteHat(hat)}>Delete</button></td>
          
            </tr>
            );
          })}
        </tbody>
      </table>
      </>
    );
  }
  
  export default HatList;

  