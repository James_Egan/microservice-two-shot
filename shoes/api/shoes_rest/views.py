from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from shoes_rest.models import Shoe, BinVO
from common.json import ModelEncoder
import json
from django.http import JsonResponse



# Create your views here.


class BinVODetailEncoder(ModelEncoder):
    model = BinVO
    properties = ["closet_name", "import_href", "bin_number", "bin_size"]





class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "model_name",
        "color",
        "picture_url",
        "id",
        "manufacturer",
        "bin"
        
    ]

    encoders = {
        "bin": BinVODetailEncoder(),
    }
    



#Hats List View, GET list and Add to list
@require_http_methods(["GET", "POST"])
def api_list_shoes(request):
    if request.method == "GET":
        shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeListEncoder,
        )
    else: #POST
        content = json.loads(request.body)
        
        try:
            href = content["bin"]
            bin = BinVO.objects.get(import_href=href)
            print(bin)
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"Message": "Invalid"},
                status = 400
            )
        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeListEncoder,
            safe=False,
        )

@require_http_methods(["DELETE"])
def delete_shoe(request, pk):
    if request.method == "DELETE":
        try:
            shoe = Shoe.objects.get(id=pk)
            shoe.delete()
            return JsonResponse(
                shoe,
                encoder=ShoeListEncoder,
                safe=False,
            )
        except Shoe.DoesNotExist:
            return JsonResponse({"message": "This Shoe Does not exist, failed to delete"})    