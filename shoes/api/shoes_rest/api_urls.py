from django.urls import path

from .views import api_list_shoes, delete_shoe

urlpatterns = [
    path("shoes/", api_list_shoes, name="api_list_shoes"),
    path("shoes/<int:pk>/", delete_shoe, name="delete_shoe"),
]